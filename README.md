# PRTG Uptime Report Generator

## What this is

A python script to call PRTG's API and create a CSV file with the uptime and downtime stats for sensors with a specific tag.

## How to use this script

Install python 3.9 if you don't have it already and create a venv.

Install the requirements and then run the script by calling it with the correct options. i.e.

`python hd-xml-csv-converter.py --tag=TAG_USED_IN_PRTG -a=http://your-prtg-url -u=USERNAME -p=PASSHASH`

Once you call this, it should produce a CSV file in the directory that's similar to this:

```csv
id,downtime,uptime
1002,1 %,99 %
1004,0 %,100 %
1003,0 %,100 %
2674,90 %,10 %
1001,0 %,100 %
1005,0 %,100 %
2673,0 %,100 %
```
