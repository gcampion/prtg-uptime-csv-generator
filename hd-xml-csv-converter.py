import csv
import json
import argparse
from xml.dom.minidom import parseString
import requests
import datetime


def converter():
    # Initialize parser
    parser = argparse.ArgumentParser()
    # Adding optional arguments
    parser.add_argument(
        "-t", "--tag", help="Tag to use for sensor search", required=True
    )
    parser.add_argument(
        "-a",
        "--address",
        help="PRTG server address i.e. http://my-prtg-server.example.com",
        required=True,
    )
    parser.add_argument("-u", "--username", help="PRTG server username ", required=True)
    parser.add_argument("-p", "--passhash", help="PRTG server passhash ", required=True)
    # Read arguments from command line
    args = parser.parse_args()

    uname = args.username
    passhash = args.passhash

    list_url = f"{args.address}/api/table.json?content=sensors&columns=sensor,objid&filter_tags=@tag({args.tag})"
    tag_list_response = send_prtg_request(list_url, uname, passhash)
    sensor_list = (json.loads(tag_list_response.content))["sensors"]

    print(sensor_list)
    uptime_list = []
    for sensor in sensor_list:
        now = datetime.datetime.utcnow()
        starting_time = now - datetime.timedelta(days=7)
        id = sensor["objid"]
        hd_url = f"{args.address}/api/historicdata_totals.xml?id={id}&sdate={starting_time}&edate={now}&avg=3600&pctavg=300&pctshow=false&pct=95&pctmode=true"
        hd_response = send_prtg_request(hd_url, uname, passhash)

        document = parseString(hd_response.text)
        uptime = document.getElementsByTagName("uptimepercent")[0].childNodes[1].data
        downtime = (
            document.getElementsByTagName("downtimepercent")[0].childNodes[1].data
        )

        uptime_list.append({"id": id, "downtime": downtime, "uptime": uptime})

    print(uptime_list)

    uptime_file = open("uptime_file.csv", "w")
    writer = csv.DictWriter(uptime_file, fieldnames=["id", "downtime", "uptime"])
    writer.writeheader()
    writer.writerows(uptime_list)
    uptime_file.close()


def send_prtg_request(
    url,
    username,
    passhash,
    timeout=10,
):
    result = requests.request(
        method="POST",
        url=url,
        params={"username": username, "passhash": passhash},
        timeout=timeout,
        allow_redirects=False,
        verify=False,
        headers={"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
    )
    return result


if __name__ == "__main__":
    converter()
